package um2.ips.web;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import um2.ips.dao.MonumentRepository;
import um2.ips.entities.Celebrite;
import um2.ips.entities.Departement;
import um2.ips.entities.Monument;
import um2.ips.metier.IMetier;

@Controller
public class Controlleur {
	// faire appel à la couche metier
	@Autowired // demander à spring :cherche moi un objet qui implemente cette interface et
				// injecte le
	private IMetier projetMonument;
//methode qui va retourner une vue par defaut
	@Autowired
	private MonumentRepository monumentRepository;

	@Secured(value = { "ROLE_ADMIN", "ROLE_USER" })
	@RequestMapping(value = "/departements") // quand je vais tapper localhost /monuments j'accede a la page
	public String index() {
		return "departements";// creer cette page monuments.html
	}
    // page Bienvenue
	@Secured(value = { "ROLE_ADMIN", "ROLE_USER" })
	@RequestMapping(value = "/")
	public String bienvenue() {
		return "Bienvenue";//
	}

//retourne la paage de gestion
	@Secured(value = { "ROLE_ADMIN" })
	@RequestMapping(value = "/Gerer")
	public String Gerer() {
		return "Gerer";//
	}

	@Secured(value = { "ROLE_ADMIN", "ROLE_USER" })
	@RequestMapping("/consultdepartement")
	public String consultercmt(Model model, String numDep) {
		try {
			Departement d = projetMonument.getDepartement(numDep);
			model.addAttribute("departement", d);
		} catch (Exception e) {
			model.addAttribute("exception", e);
		}
		return "departements";
	}
//saisie des monuments
//quand on va utiliser le model
	@Secured(value = { "ROLE_ADMIN" })
	@RequestMapping(value = "/formMonument", method = RequestMethod.GET)
	public String formMonument(Model model) {
		// ce formulaire permet de creer un monument
		// comme j'écris ca je suis sensé voir ces informations dans le formulaire car
		// j'ai fait data binding
//	model.addAttribute("monument",new Monument("xxma","placeComedie","tassadit","statut",1.0f,1.0f, null));
		model.addAttribute("monument", new Monument());
		return "formMonument";
	}
//maintenat je vais creer l'action  SaveMonument
	@Secured(value = { "ROLE_ADMIN" })
	@RequestMapping(value = "/SaveMonument", method = RequestMethod.POST)
//quand j'appelle saveMonument au moment de l'execution du formulaire 
	public String save(Monument m) {
		// dispatcher servelete vas stoquer toutes données de la requete dans un objet
		// monument
		// si il trouve dans la requete http un attribut qui s'appelle codeM il vas
		// chercher dans un variable
		// si on a ça
		// ensuite je vais envoyer les données vers ma bdd
		monumentRepository.save(m);
		return "sucess";
	}

//afficher la liste des monuments 
	@Secured(value = { "ROLE_ADMIN"})
	@RequestMapping(value = "/monuments")
	public String monuments(Model model) {
		List<Monument> m = monumentRepository.findAll();
		model.addAttribute("monuments", m);
		return "monuments";
	}

	@Secured(value = { "ROLE_ADMIN", "ROLE_USER" })
	@RequestMapping(value = "/rechercher")

	public String rechercher(Model model, @RequestParam(name = "mc", defaultValue = " ") String mc) {
		List<Monument> m = projetMonument.FindMonument(mc);
		model.addAttribute("listm", m);
		model.addAttribute("mc", mc);

		return "monumentV";
	}

	@Secured(value = { "ROLE_ADMIN", "ROLE_USER" })
	@RequestMapping(value = "/distance") //
	public String dist() {
		return "distance";//
	}

//calcule de la distance entre deux monuments
	@Secured(value = { "ROLE_ADMIN", "ROLE_USER" })
	@RequestMapping(value = "/calculeDistance")
	public String calculeDistance(Model model, String codeMA, String codeMB) {
		Monument mA = monumentRepository.getOne(codeMA);
		Monument mB = monumentRepository.getOne(codeMB);
		float r = projetMonument.getDistanceBetweenMonuments(codeMA, codeMB);
		model.addAttribute("result", r);

		return "distance";
	}

// supprimer un monument
	@Secured(value = { "ROLE_ADMIN" })
	@RequestMapping(value = "/supprimerM")
	public String supprimerM(String codeM) {
		projetMonument.deleteMonument(codeM);
		return "sucess";
		
	}

//modifier un monument 
	@Secured(value = { "ROLE_ADMIN" })
	@RequestMapping(value = "/ModifierM")
	public String ModifierM(Model model, String codeM) {
		Monument m = monumentRepository.getOne(codeM);
		// recuperer un monument et le stocker dans le model
		model.addAttribute("monument", m);
		return "EditMonument";
	}

	@Secured(value = { "ROLE_ADMIN" })
	@RequestMapping(value = "/updateMonument", method = RequestMethod.POST)
//quand j'appelle saveMonument au moment de l'execution du formulaire 
	public String update(Monument m) {
		monumentRepository.save(m);// si on lui donne un monumet qui existe déja elle fait update
		return "sucess";
	}

//retourne le formulaire qui associe une celebrite à un monument
	@Secured(value = { "ROLE_ADMIN" })
	@RequestMapping(value = "/Associer")
	public String formAsso() {
		return "AssossierCelebToMon";
	}

	@Secured(value = { "ROLE_ADMIN" })
	@RequestMapping(value = "/AssocierM")
	public String AddCelebriteToMonumen(Model model, String numCelebrite, String CodeM) {
		projetMonument.AddCelebriteToMonument(numCelebrite, CodeM);
		// Monument m =new Monument();
		// model.addAttribute("Monumentm", m);
		return "sucess";
	}

//me retourne la page listMonumentByDep
	@Secured(value = { "ROLE_ADMIN", "ROLE_USER" })
	@RequestMapping(value = "/listMonumentbydep")
	public String listemon() {
		return "ListMonumentByDep";
	}

	// afficher les monuments d'un departement
	@Secured(value = { "ROLE_ADMIN", "ROLE_USER" })
	@RequestMapping(value = "/listMonbydep")
	public String listmon(Model model, String dep) {
		try {
			Collection<Monument> listmon = projetMonument.getListMonumentsByDep(dep);
			model.addAttribute("list", listmon);

		} catch (Exception e) {
			model.addAttribute("exception", e);
		}
		return "ListMonumentByDep";
   }

	// me retourne la page Listcelebritebymonument
	@Secured(value = { "ROLE_ADMIN", "ROLE_USER" })
	@RequestMapping(value = "/ListcelebritebyMon")
	public String pagelistcelebAssocierM() {
		return "ListcelebritebyMon";
	}

	// recupere la liste des celebrites associées à un monument
	@Secured(value = { "ROLE_ADMIN", "ROLE_USER" })
	@RequestMapping(value = "/listCeleb")
	public String listcelebAssocierM(Model model, String codeM) {
		try {
			Collection<Celebrite> listcele = projetMonument.getlisCelebritetByMon(codeM);
			model.addAttribute("listc", listcele);

		} catch (Exception e) {
			model.addAttribute("exception", e);
		}
		return "ListcelebritebyMon";
	}

	// Monument pour visiteur
	@Secured(value = { "ROLE_ADMIN", "ROLE_USER" })
	@RequestMapping(value = "/monumentV")
	public String monumentv(Model model) {
		List<Monument> m = monumentRepository.findAll();
		model.addAttribute("monuments", m);
		return "monumentV";
	}
///recuperer la session de l'utilisateur 
//	@Secured(value = { "ROLE_ADMIN", "ROLE_USER" })
//	@RequestMapping(value="/getLogUser")
//		public Map<String,Object >getLogUser(HttpServletRequest httpServeletRequest){
//		HttpSession httpSession=httpServeletRequest.getSession();
//		//recuperer le contexte de securite
//		SecurityContext securitcontext =(SecurityContext) httpSession.getAttribute("SPRING_SECURITY_CONTEXT");
//		//recuperer le nom de l'utilisateur
//		String username = securitcontext.getAuthentication().getName();
//		List<String> roles=new ArrayList<>();
//		for(GrantedAuthority ga :securitcontext.getAuthentication().getAuthorities()) {
//				roles.add(ga.getAuthority());
//			}
//		Map<String,Object>params =new HashMap<>();
//		params.put("username", username);
//		params.put("roles",roles);
//		return params;}
			
	}
	

	
	
	
	
	
