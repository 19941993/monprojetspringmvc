package um2.ips.dao;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import um2.ips.entities.Departement;
import um2.ips.entities.Monument;



public interface MonumentRepository extends JpaRepository<Monument,String> {
	//Spring quand il demarre et il detecte une interface qui herite de jpaRepository il vas automatiquement
	//injecter une implementation de cette interface
	//avec Spring Data JPA on a toutes les methodes classiques 
	
		//trouver un monument par son nom
		//il suffit de spécifier l'annotation query :on lui donne la requete à exécuter
	    //@Query("select m from Monument m where m.nomM like:x")
		//public Page <Monument>FindMonument(@Param("x")String mc,Pageable pageable);
		
		@Query("select m from Monument m where m.nomM like:x")
		public List <Monument> FindMonument(@Param("x")String mc);
	   
		@Query("select m from Monument m where m.codeM=:x")
		public Monument getMonument(@Param("x")String codeM);
}
