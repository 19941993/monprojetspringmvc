package um2.ips.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import um2.ips.entities.Departement;
import um2.ips.entities.Lieu;

public interface LieuRepository extends JpaRepository<Lieu, String> {
	// on a toutes les methodes classiques grace Spring data JPA
	// trouver un lieu par nom
	@Query("select l from Lieu l where  l.nomCom like:x")
	public List<Departement> FindLieuParnomCom(@Param("x") String mc);

	// @Query("delete l from Lieu l where l.codeInsee like:x")
	// public void deleteLieu(@Param("x")String codeInsee);

	@Query("select l from Lieu l where l.codeInsee=:x")
	public Lieu getLieu(@Param("x") String codeInsee);
}
