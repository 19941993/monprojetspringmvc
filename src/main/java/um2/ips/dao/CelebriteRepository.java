package um2.ips.dao;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import um2.ips.entities.Celebrite;
import um2.ips.entities.Departement;


public interface CelebriteRepository extends JpaRepository<Celebrite,String>  {
	
	
	//avec Spring Data JPA on a toutes les methodes classiques 
	
	//trouver des celebrites par nom
	//il suffit de spécifier l'annotation query :on lui donne la requete à exécuter
	@Query("select c from Celebrite c where  c.nom like:x")
	public List <Celebrite> FindcelebriteParNom(@Param("x")String mc);
   
	@Query("select c from Celebrite c where c.numCelebrite=:x")
	public Celebrite getCelebrite(@Param("x")String numCelebrite);
}
