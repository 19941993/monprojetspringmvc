package um2.ips.metier;
import java.util.ArrayList;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.management.relation.RoleInfoNotFoundException;

import org.apache.lucene.util.SloppyMath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import um2.ips.dao.CelebriteRepository;
import um2.ips.dao.DepartementRepository;
import um2.ips.dao.LieuRepository;
import um2.ips.dao.MonumentRepository;
import um2.ips.entities.Celebrite;
import um2.ips.entities.Departement;
import um2.ips.entities.Lieu;
import um2.ips.entities.Monument;
@Service
@Transactional
public class metier implements IMetier {
	@Autowired //demander à spring d'injecter cette implementation
	private MonumentRepository MonumentRepository;
	@Autowired
	private LieuRepository LieuRepository;
	@Autowired
	private DepartementRepository DepartementRepository;
	@Autowired
	private CelebriteRepository CelebriteRepository;
	
	
	
	@Override
	public Lieu addLieu(Lieu l) {
	LieuRepository.save(l);
	
	return l;

		
	}

	@Override
	public Departement addDepartement(Departement Dep) {
	DepartementRepository.save(Dep);
	return Dep;
	}

	@Override
	public Monument addMonument(Monument m) {
	MonumentRepository.save(m);
		return m;
	}


	@Override
	public Celebrite addCelebrite(Celebrite c) {
		CelebriteRepository.save(c);
		return c;
	}

	@Override
	public Departement getDepartement(String numDep) {
		return DepartementRepository.getDepartement(numDep);
		
			
	}
	@Override
	public Monument getMonument(String CodeM) {
	return MonumentRepository.getMonument(CodeM);
	}
	@Override
	public Celebrite getCelebrite(String numCelebrite) {
		return CelebriteRepository.getCelebrite(numCelebrite);
	}
	@Override
	public Lieu getLieu(String codeInsee) {
		return LieuRepository.getLieu(codeInsee);
	}


	
	@Override
	public List<Departement> getListDepartements() {
		List<Departement> dep =DepartementRepository.findAll();
		return dep;
	}

	@Override
	public List<Lieu> getListLieux() {
		List<Lieu> lieux = LieuRepository.findAll();
		return lieux;
	}

	@Override
	public void deleteLieu(String codeInsee) {
		Lieu l=LieuRepository.getOne(codeInsee);
		LieuRepository.delete(l);
		
			
	}

	@Override
	public void addMonumentToLieu(String codeM, String codeInsee) {
		Monument m=MonumentRepository.getOne(codeM);
		Lieu l=LieuRepository.getOne(codeInsee);
		l.getMonuments().add(m);
		}
	

	@Override
	public Collection<Monument> getListMonumentsByDep(String dep) {
		Departement d = DepartementRepository.getDepartement(dep);
		Collection<Lieu> l = d.getLieux();
		Collection<Monument> res = new ArrayList<>();
		for(Lieu c:l)
			for(Monument m:c.getMonuments())
				res.add(m);			
		return res;
	}
	

	@Override
	public Collection<Monument> getListMonumentsByLieu(String codeInsee) {
		Lieu l = LieuRepository.getLieu(codeInsee);
		return l.getMonuments();
	}

	@Override
	public void AddCelebriteToMonument(String numCelebrite, String CodeM) {
		Monument mo=MonumentRepository.getOne(CodeM);
		Celebrite c =CelebriteRepository.getOne(numCelebrite);
		c.getMonuments().add(mo);
		mo.getCelebrites().add(c);
		
	}

	@Override
	public void deleteDepartement(String numDep) {
		Departement d = DepartementRepository.getOne(numDep);
		DepartementRepository.delete(d);
		
	}

	@Override
	public void deleteMonument(String codeM) {
		Monument m=MonumentRepository.getOne(codeM);
		MonumentRepository.delete(m);
		
	}

	@Override
	public void deleteCelebrite(String numCelebrite) {
		Celebrite c =CelebriteRepository.getOne(numCelebrite);
		CelebriteRepository.delete(c);
	}

	@Override
	public List<Monument> FindMonument(String mc) {
		return MonumentRepository.FindMonument(mc);
	}

	@Override
	public float getDistanceBetweenMonuments(String codeMA, String codeMB) {
		    float latMa = MonumentRepository.getOne(codeMA).getLatitude();
			float longMa=MonumentRepository.getOne(codeMA).getLongitude();
			float latMb=MonumentRepository.getOne(codeMB).getLatitude();
			float longiMb = MonumentRepository.getOne(codeMB).getLongitude();
		float dist= (float) SloppyMath.haversinKilometers(latMa, longMa, latMb, longiMb);
			return dist;
		}

	@Override
	public Collection<Celebrite> getlisCelebritetByMon(String codeM) {
		Monument m=MonumentRepository.getMonument(codeM);
		return m.getCelebrites();
	
	}

	
			
		}

	
	

	

	
	



