package um2.ips.metier;

import java.util.Collection;
import java.util.List;

import um2.ips.entities.Celebrite;
import um2.ips.entities.Departement;
import um2.ips.entities.Lieu;
import um2.ips.entities.Monument;


public interface IMetier {
	//logique metier
	// ADD
	public Lieu addLieu(Lieu l);
	public Departement addDepartement(Departement d);
	public Monument addMonument(Monument m);
	public Celebrite addCelebrite(Celebrite c);
	//get
	public Departement getDepartement(String numDep);
	public Monument getMonument(String codeM);
	public Celebrite getCelebrite(String numCelebrite);
	public Lieu getLieu(String codeInsee);
	public List<Departement> getListDepartements();
	public List<Lieu> getListLieux();
	public List<Monument> FindMonument(String mc);
	
	//delete
	public void deleteLieu(String codeInsee);
	public void deleteMonument(String codeM);
	public void deleteCelebrite(String numCelebrite);
	void deleteDepartement(String numDep);
	//c'est un peu bizzare de supprimer toute un departement
	
	
	public void addMonumentToLieu(String codeM, String codeInsee);
	public float getDistanceBetweenMonuments(String codeMA,String codeMB);
	public Collection<Monument> getListMonumentsByDep(String dep);
	public Collection<Monument> getListMonumentsByLieu(String codeInsee);
	public void AddCelebriteToMonument(String numCelebrite,String CodeM);
	public Collection<Celebrite> getlisCelebritetByMon(String codeM) ;
	

}
