package um2.ips.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import um2.ips.entities.Departement;

public interface DepartementRepository extends JpaRepository<Departement, String> {
	// on a toutes les methodes classiques grace Spring data JPA
	@Query("select d from Departement d where  d.nomDep like:x")
	public List<Departement> FindDepartementParNomDep(@Param("x") String mc);

	@Query("select d from Departement d where d.numDep=:x")
	public Departement getDepartement(@Param("x") String numDep);

}
