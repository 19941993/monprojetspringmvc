package um2.ips;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import um2.ips.dao.CelebriteRepository;
import um2.ips.dao.DepartementRepository;
import um2.ips.dao.LieuRepository;
import um2.ips.dao.MonumentRepository;
import um2.ips.entities.Celebrite;
import um2.ips.entities.Departement;
import um2.ips.entities.Lieu;
import um2.ips.entities.Monument;
import um2.ips.metier.IMetier;

@SpringBootApplication
public class MonProjetSpringMvcApplication implements CommandLineRunner {
	@Autowired
	private CelebriteRepository celebriteRepository;
	@Autowired
	private DepartementRepository departementRepository;
	@Autowired
	private LieuRepository lieuRepository;
	@Autowired
	private MonumentRepository monumentRepository;
	@Autowired
	private IMetier imetier;

	public static void main(String[] args) {
		SpringApplication.run(MonProjetSpringMvcApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		// ajouter des Départements
		Departement d = new Departement("xxdz", "tassadit", "herault", "Montpellir");

		Departement d1 = departementRepository.save(new Departement("xxda", "tassadit", "herault", "Montpellir"));
		Departement d2 = departementRepository.save(new Departement("xxdb", "tassadi", "herau", "Montpelier"));
		Departement d3 = departementRepository.save(new Departement("xxdc", "tassad", "her", "Montpeier"));
		Departement d4 = departementRepository.save(new Departement("xxdd", "tassa", "her", "Montpelier"));
		// ajouter des lieux

		Lieu l1 = lieuRepository.save(new Lieu("XXla", "herault", 1.0f, 11.0f, d1));
		Lieu l2 = lieuRepository.save(new Lieu("XXlb", "herault", 1.0f, 11.0f, d2));
		Lieu l3 = lieuRepository.save(new Lieu("XXlc", "herault", 1.0f, 11.0f, d1));
		Lieu l4 = lieuRepository.save(new Lieu("XXld", "herault", 1.0f, 11.0f, d3));

		// ajouter des Monuments

		Monument m1 = monumentRepository
				.save(new Monument("xxma", "placeComedie", "tassadit", "statut", 1.0f, 1.0f, l1));
		Monument m2 = monumentRepository
				.save(new Monument("xxmb", "placeComedie", "tassadit", "statut", 1.0f, 1.0f, l1));
		Monument m3 = monumentRepository
				.save(new Monument("xxmc", "placeComedie", "tassadit", "statut", 1.0f, 1.0f, l1));
		Monument m4 = monumentRepository
				.save(new Monument("xxmd", "placeComedie", "tassadit", "statut", 1.0f, 1.0f, l4));

		// ajouter des ceebrites

		Celebrite c1 = celebriteRepository.save(new Celebrite("bbb", "benAhmed", "tassadit", "algerienne", "recent"));
		Celebrite c2 = celebriteRepository.save(new Celebrite("vvv", "Djebrouni", "Malika", "algerienne", "recent"));
		Celebrite c3 = celebriteRepository.save(new Celebrite("nnn", "karkour", "nazihh", "algerien", "recent"));
		Celebrite c4 = celebriteRepository.save(new Celebrite("kkk", "nazih", "Nazih", "algerien", "recent"));

		// trouver les Celebrites qui commencenet par "t"
		// List<Celebrite> c = celebriteRepository.FindcelebriteParNom("%Dj%");
		// for(Celebrite ce:c) {
		// System.out.println("des:"+c1.getNom());
		
		// supprimer une celibrite par id
		// celebriteDao.deleteById(1);
		// afficher le nom de chaque celebrite
		
		// celebriteDao.findAll().forEach(ce->System.out.println(ce.getNom()));
		// trouver un département par codem

		// System.out.println(departementRepository.getDepartement("xxdb").getNomDep());
		// System.out.println(imetier.getDepartement("xxdb").getNomDep());
		// supprimer un lieu

		// imetier.deleteLieu("xxmd");
		// lieuRepository.deleteById("XXlb");
		// imetier. deleteLieu("XXld");
		// trouver tous les lieux
		// imetier.getListLieux().forEach(li->System.out.println(li.getCodeInsee()));
		// ajouter un monument à un lieu
		// imetier.addMonumentToLieu("xxmb","XXlc");
		// monumentRepository.getListMonumentsByDep("herault");

//		//liste des monuments qui ont la lettre p
//			List <Monument> mo=imetier.FindMonument("%p%");
//			mo.forEach(m->System.out.println(m.getNomM()));
		// associer une celebrite à un monument

		// Calculer la distance entre deux monuments
		// System.out.println(imetier.getDistanceBetweenMonuments("xxma", "xxma"));

		imetier.AddCelebriteToMonument("vvv", "xxmb");
		imetier.AddCelebriteToMonument("nnn", "xxmc");
		imetier.AddCelebriteToMonument("kkk", "xxmd");

		// voir toutes les celebrites associées à un monument
		// Collection <Celebrite> co =imetier.getCelebretiesbyMonum("xxma");

		// co.forEach(c->System.out.println(c.getNom()));

		// recuperer la liste des monuments associer à un departement
		// Collection<Monument> m= imetier.getListMonumentsByDep("xxda");
		// m.forEach(mo->System.out.println(mo.getNomM()));

		// recuperer la liste des celebrites associées à un monument si elles exitent
//    Collection <Celebrite> c=imetier.getlisCelebritetByMon("xxmb"); 
//    c.forEach(ce->System.out.println(ce.getNumCelebrite()));

	}
}
